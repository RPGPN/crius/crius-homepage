module.exports = {
    purge: [
        "layouts/**/*.html"
    ],
    theme: {
        extend: {
            colors: {
                primary: "#EA3546",
                secondary: "#F86624"
            },
            fontFamily: {
                sans: ['Montserrat', "Avenir Next", "Avenir", "Helvetica Neue", "sans-serif"],
                display: ["Josefin Sans", "Avenir Next", "Avenir", "Helvetica Neue", "sans-serif"]
            },
            borderRadius: {
                "large": "16px"
            }
        },
    },
    variants: {},
    plugins: [
        require('@tailwindcss/typography'),
    ]
}
