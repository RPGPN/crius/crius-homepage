---
title: "Support | RPGPN Crius"
---

# Support

If you need help with RPGPN Crius, you can join our [Discord](https://discord.gg/XfeWS6vs3d) and ask in the #rpgpn-crius
channel. You can also tweet [@rpgpnmedia](https://twitter.com/rpgpnmedia) or [@ponkey364](https://twitter.com/ponkey364).
If you prefer email, you can email crius[@]rpgpn[.]co[.]uk and someone will get back to you.