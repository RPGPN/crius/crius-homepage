---
title: "LiveCrius Changelog - RPGPN Crius"
---

> Last updated: 2021-03-29 by ponkey364

## beta2103d
**this build contains experimental features from the `prometheus-metrics` branch**
## Features
- Now using subcommands for NoLinks and Timeouts, please check the [commands](/docs/commands) list for changes. 

## Plugin updates
- All plugins apart from BotInfo have been updated to use Commander v2.4
- crius-plugin-info now uses subcommands, please check the [commands](/docs/commands) list for changes.

### Other
- Commander v2.4
  - Subcommands


## beta2103c
**this build contains experimental features from the `prometheus-metrics` branch**
### Features
- Allow for customising the NoLinks warning message

### Bug Fixes
- APIFuncs caching funcs took a cache param instead of using the value on apifuncs

### Other
- Commander v2.3.1
    - Show in help flag with PluginInfo
    - should recover() from panic()


## beta2103b (2021-03-09)
**this build contains experimental features from the `prometheus-metrics` branch**
### Features
- Prometheus (see above)
- Timed messages

### Other
- Crius Utils updates
    - new settings interface with just GetDB which can be used for both LC & GC
- Commander (v2.3)
    - Guards
    - allow for sending messages without having a MessageContext
    - Expand MessageContext

## beta2103a (2021-03-02)
**this build contains experimental features from the `prometheus-metrics` branch**
### Features
- Polls plugin
- Have nolink be opt-in
- Prometheus (see above)

### Bug Fixes
- SetWarnCount returns usage for SetWarnDur

### Plugin Updates
- Quotes
    - quotes can return a quote from another realm/platform
    
## alpha2102a (2021-02-26)
### Features
- Quotes plugin
- NoLinks timeouts
- NoLinks caching
- Info plugin

### Plugin Updates
- Links
    - Ensure a link gets passed in to newliink

### Other
- CriusCommander updates
    - fix cc bug when message == prefix
- Crius Utils updates
    - more nicer accessors
- golesh-chat updates
    - fix the bug that stopped golesh-chat from workiing