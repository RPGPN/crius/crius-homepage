---
title: "RPGPN Crius"
---

{{< hey "Crius" "by RPGPN" >}}

Crius is the name of a pair of bots based off of our [Crius Commander](https://gitlab.com/crius-bots/criuscommander) library.

There was LiveCrius (now inactive), for Livestreaming platforms like Twitch and Glimesh, and GuildCrius, for Discord.

Crius is currently in maintainance mode while we work on something new - for Discord server administrators who use Crius: do not fear, everything still works and will continue to until we build a replacement (mostly because I use it personally on a few servers)
